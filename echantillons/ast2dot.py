import ast
from collections import defaultdict


class AstGraphGenerator(object):
    def __init__(self):
        self.graph = defaultdict(lambda: [])

    def __str__(self):
        return str(self.graph)

    def visit(self, node):
        """Visit a node."""
        method = "visit_" + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        return visitor(node)

    def generic_visit(self, node):
        """Called if no explicit visitor function exists for a node."""
        for _, value in ast.iter_fields(node):
            if isinstance(value, list):
                for item in value:
                    if isinstance(item, ast.AST):
                        self.visit(item)

            elif isinstance(value, ast.AST):
                self.graph[type(node)].append(type(value))
                self.visit(value)


if __name__ == "__main__":
    code = '''def maximum(nombre_1, nombre_2, nombre_3):
    """
    Ma super fonction maximum qui renvoie le plus grand 
    des trois nombres qu'on lui donne en entrée.
    """
    return max([nombre_1,nombre_2, nombre_3])

# un petit test :
def test_maximum():
    assert maximum(2,3,-1) == 3'''
    generateur = AstGraphGenerator()

    print()
