#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ghostscript

args = [
    "-sDEVICE=jpeg",
    "-sOutputFile=test.jpg",
     "-sIntputFile=echantillons/test.pdf"
]

ghostscript.Ghostscript(b"-dNOPAUSE -sDEVICE=jpeg -r200 -dJPEGQ=60 -sOutputFile=echantillons/test.jpg echantillons/test.pdf -dBATCH")
